<?php

namespace Gdev\SignatureGenerator;


interface SignatureInterface
{

    public function getFont($font);

    public function getImageJPG();

    public function draw();
}