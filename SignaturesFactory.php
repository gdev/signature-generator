<?php

namespace Gdev\SignatureGenerator;

class SignaturesFactory
{

    public static function Create($signature) {

        $signatureClass = "\\Gdev\\SignatureGenerator\\" . $signature . "Signature";

        if(!class_exists($signatureClass)) {
                 throw new \Exception("Signature $signature does not exist");
        }
        $signatureClass = new $signatureClass();

        if(!($signatureClass instanceof SignatureInterface)) {
            throw new \Exception("Signature $signature does not implement FontInterface");
        }

        return $signatureClass;
    }

}
