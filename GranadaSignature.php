<?php

namespace Gdev\SignatureGenerator;

class GranadaSignature implements SignatureInterface
{

    private $signature;
    private $colors;
    private $fonts;

    private $data;

    public function __construct()
    {
        $this->signature = imagecreatefromjpeg($this->getImageJPG());
        $this->colors = (object)[
            'white' => imagecolorallocate($this->signature, 255, 255, 255),
            'red' => imagecolorallocate($this->signature, 255, 22, 73),
        ];

        $this->fonts = (object)[
            'regular' => $this->getFont('Mercury-Roman'),
            'semibold' => $this->getFont('Mercury-SemiBold'),
            'bold' => $this->getFont('Mercury-Bold')
        ];
    }

    public function setData($name, $title, $phone)
    {
        $this->data = (object)[
            'name' => $name,
            'title' => $title,
            'phone' => $phone
        ];
    }

    public function getFont($font)
    {
        return __DIR__ . '/Fonts/' . $font . '.ttf';
    }

    public function getImageJPG()
    {
        return __DIR__ . '/Images/festivalDeGranada.jpg';
    }

    public function draw()
    {
        //Font Parametars
        $fontSizeText1 = 11.5;
        $fontSizeText2 = 9;
        $fontSizeText3 = 11.5;
        $textAnge = 0;
        $fromTheRight = 243;
        $fromTheTopText1 = 26;
        $fromTheTopText2 = 42;
        $fromTheTopText3 = 59;

        //Text to Image
        imagettftext($this->signature, $fontSizeText1, $textAnge, $fromTheRight, $fromTheTopText1, $this->colors->red, $this->fonts->semibold, ucwords($this->data->name));
        imagettftext($this->signature, $fontSizeText2, $textAnge, $fromTheRight, $fromTheTopText2, $this->colors->red, $this->fonts->regular, strtoupper($this->data->title));
        imagettftext($this->signature, $fontSizeText3, $textAnge, $fromTheRight, $fromTheTopText3, $this->colors->red, $this->fonts->regular, $this->data->phone);
        header('Content-type: image/jpeg');
        imagejpeg($this->signature, null, 100);
        imagedestroy($this->signature);

    }


}